.. ctitools documentation master file, created by
   sphinx-quickstart on Mon Jul 18 18:07:36 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. include:: ../README.rst

Usage
=====

.. sphinx_argparse_cli::
  :module: berhoel.ctitools.cti2bibtex
  :func: build_parser
  :prog: cli2bibtex
  :no_default_values:

.. sphinx_argparse_cli::
  :module: berhoel.ctitools
  :func: __build_cti_statistics_parser
  :prog: cti_statistics
  :no_default_values:

API documentation
=================

.. currentmodule:: berhoel

.. autosummary::
   :nosignatures:

   ctitools
   ctitools.ix
   ctitools.ct
   ctitools.ctientry
   ctitools.cti2bibtex

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   _tmp/modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
